<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('coffees', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_id')->unique();
            $table->string('category_name');
            $table->string('sku');
            $table->string('name');
            $table->text('description');
            $table->text('shortdesc');
            $table->decimal('price', 8, 2);
            $table->string('link');
            $table->string('image');
            $table->string('brand');
            $table->decimal('rating', 3, 2);
            $table->string('caffeine_type');
            $table->integer('count');
            $table->boolean('flavored');
            $table->boolean('seasonal');
            $table->boolean('instock');
            $table->boolean('facebook');
            $table->boolean('is_kcup');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down():void
    {
        Schema::dropIfExists('coffees');
    }
};
