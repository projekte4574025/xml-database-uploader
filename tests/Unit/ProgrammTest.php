<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Implementations\DataStorageDB;
use App\Models\Coffee;
use App\Implementations\DataSourceLocalXML;

class ProgrammTest extends TestCase
{
    use RefreshDatabase;

    protected $dataStorage;
    protected $dataSource;

    /**
     * Set up the test environment.
     */
    public function setUp(): void
    {
        parent::setUp(); 
        $this->dataStorage = new DataStorageDB();
        $this->dataSource = new DataSourceLocalXML();
    }

    /**
     * Test retrieving data from the XML source.
     */
    public function testGetData()
    {
        $filePath = __DIR__ . '/test.xml';
        $expected = $this->getExpectedData();
        $dataArray = $this->dataSource->getData($filePath);

        foreach ($dataArray as $index => $coffee) {
            $this->assertCoffeeEquals($expected[$index], $coffee);
        }
    }

    /**
     * Get the expected data for comparison.
     * 
     * @return array
     */
    public function getExpectedData()
    {
        return [
            $this->createCoffee('340', 'Green Mountain Ground Coffee', '20', 'Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag', '', 'Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag steeps cup after cup of smoky-sweet, complex dark roast coffee from Green Mountain Ground Coffee.', 41.6000, 'http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html', 'http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg', 'Green Mountain Coffee', 0, 'Caffeinated', 24, false, false, true, true, false),
            $this->createCoffee('341', 'Starbucks Ground Coffee', '21', 'Starbucks Coffee Pike Place Roast Ground Coffee 12 2.5oz Bag', '', 'Starbucks Coffee Pike Place Roast Ground Coffee 12 2.5oz Bag offers a smooth, well-rounded blend of Latin American coffees with subtly rich flavors of cocoa and toasted nuts.', 35.5000, 'http://www.coffeeforless.com/starbucks-coffee-pike-place-roast-ground-coffee-12-2-5oz-bag.html', 'http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg', 'Starbucks Coffee', 4.5, 'Caffeinated', 12, false, false, true, true, false),
            $this->createCoffee('342', 'Dunkin\' Donuts Ground Coffee', '22', 'Dunkin\' Donuts Original Blend Ground Coffee 10 1lb Bag', '', 'Dunkin\' Donuts Original Blend Ground Coffee 10 1lb Bag brings the rich, smooth taste of Dunkin\' Donuts coffee to your home.', 49.9900, 'http://www.coffeeforless.com/dunkin-donuts-original-blend-ground-coffee-10-1lb-bag.html', 'http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg', 'Dunkin\' Donuts', 5, 'Caffeinated', 10, false, false, true, true, false)
        ];
    }

    /**
     * Create a Coffee object with the given properties.
     * 
     * @return Coffee
     */
    public function createCoffee($entity_id, $CategoryName, $sku, $name, $description, $shortdesc, $price, $link, $image, $Brand, $Rating, $CaffeineType, $Count, $Flavored, $Seasonal, $Instock, $Facebook, $IsKCup)
    {
        $coffee = new Coffee();
        $coffee->entity_id = $entity_id;
        $coffee->CategoryName = $CategoryName;
        $coffee->sku = $sku;
        $coffee->name = $name;
        $coffee->description = $description;
        $coffee->shortdesc = $shortdesc;
        $coffee->price = $price;
        $coffee->link = $link;
        $coffee->image = $image;
        $coffee->Brand = $Brand;
        $coffee->Rating = $Rating;
        $coffee->CaffeineType = $CaffeineType;
        $coffee->Count = $Count;
        $coffee->Flavored = $Flavored;
        $coffee->Seasonal = $Seasonal;
        $coffee->Instock = $Instock;
        $coffee->Facebook = $Facebook;
        $coffee->IsKCup = $IsKCup;

        return $coffee;
    }

    /**
     * Assert that two Coffee objects are equal.
     * 
     * @param Coffee $expected
     * @param Coffee $actual
     */
    public function assertCoffeeEquals(Coffee $expected, Coffee $actual)
    {
        $this->assertInstanceOf(Coffee::class, $actual);
        $this->assertEquals($expected->entity_id, $actual->entity_id);
        $this->assertEquals($expected->CategoryName, $actual->CategoryName);
        $this->assertEquals($expected->sku, $actual->sku);
        $this->assertEquals($expected->name, $actual->name);
        $this->assertEquals($expected->description, $actual->description);
        $this->assertEquals($expected->shortdesc, $actual->shortdesc);
        $this->assertEquals($expected->price, $actual->price);
        $this->assertEquals($expected->link, $actual->link);
        $this->assertEquals($expected->image, $actual->image);
        $this->assertEquals($expected->Brand, $actual->Brand);
        $this->assertEquals($expected->Rating, $actual->Rating);
        $this->assertEquals($expected->CaffeineType, $actual->CaffeineType);
        $this->assertEquals($expected->Count, $actual->Count);
        $this->assertEquals($expected->Flavored, $actual->Flavored);
        $this->assertEquals($expected->Seasonal, $actual->Seasonal);
        $this->assertEquals($expected->Instock, $actual->Instock);
        $this->assertEquals($expected->Facebook, $actual->Facebook);
        $this->assertEquals($expected->IsKCup, $actual->IsKCup);
    }

    /**
     * Test to add a coffee item to the database.
     */
    public function testUploadToDB()
    {
        $filePath = __DIR__ . '/test.xml';
        $dataArray = $this->dataSource->getData($filePath);
        $this->dataStorage->UploadToDB($dataArray);

        $expectedData = $this->getExpectedData();
        foreach ($expectedData as $expected) {
            $this->assertDatabaseHas('coffees', [
                'entity_id' => $expected->entity_id,
                'category_name' => $expected->CategoryName,
                'sku' => $expected->sku,
                'name' => $expected->name,
                'description' => $expected->description,
                'shortdesc' => $expected->shortdesc,
                'price' => $expected->price,
                'link' => $expected->link,
                'image' => $expected->image,
                'brand' => $expected->Brand,
                'rating' => $expected->Rating,
                'caffeine_type' => $expected->CaffeineType,
                'count' => $expected->Count,
                'flavored' => $expected->Flavored,
                'seasonal' => $expected->Seasonal,
                'instock' => $expected->Instock,
                'facebook' => $expected->Facebook,
                'is_kcup' => $expected->IsKCup,
            ]);
        }
    }
}
