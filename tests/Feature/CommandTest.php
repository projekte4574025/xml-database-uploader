<?php

namespace Tests\Feature;

use Tests\TestCase;

class CommandTest extends TestCase
{
    /**
     * Test if the command executes.
     *
     * @return void
     */
    public function test_my_command(): void
    {
        $this->artisan('importer file')
            ->expectsOutput('Command is being executed.')
            ->assertExitCode(0);
    }
}