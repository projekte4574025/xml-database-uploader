**Project Setup**

-This PHP Laravel project functions out of the box (Requires installed PHP and Composer). If you wish to use your own databases, modify the .env or .env.testing file accordingly. 

-You can replace 'public/feed.xml' with the path to your XML file stored anywhere. 

-To set up the project, follow these steps:

# Setup the programm:
composer install

# Setup the database:
php artisan migrate 

# Run command programm 
php artisan importer public/feed.xml


# Setup the test-database:
php artisan migrate --env=testing

# Run tests
php artisan test