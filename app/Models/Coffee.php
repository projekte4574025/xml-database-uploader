<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coffee extends Model
{
    use HasFactory;

    protected $fillable = [
        'entity_id', 'category_name', 'sku', 'name', 'description', 'shortdesc', 'price', 
        'link', 'image', 'brand', 'rating', 'caffeine_type', 'count', 'flavored', 
        'seasonal', 'instock', 'facebook', 'is_kcup'
    ];

    protected $casts = [
        'entity_id' => 'integer',
        'price' => 'decimal:2',
        'rating' => 'decimal:2',
        'count' => 'integer',
        'flavored' => 'boolean',
        'seasonal' => 'boolean',
        'instock' => 'boolean',
        'facebook' => 'boolean',
        'is_kcup' => 'boolean',
    ];
}