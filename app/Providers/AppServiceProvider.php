<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\DataSourceInterface;
use App\Implementations\DataSourceLocalXML;
use App\Interfaces\DataStorageInterface;
use App\Implementations\DataStorageDB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(DataSourceInterface::class, DataSourceLocalXML::class);
        $this->app->bind(DataStorageInterface::class, DataStorageDB::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
