<?php

namespace App\Implementations;

use App\Interfaces\DataSourceInterface;
use App\Models\Coffee;

/**
 * Class DataSourceLocalXML
 *
 * Implementation of DataSourceInterface for local XML data sources.
 */
class DataSourceLocalXML implements DataSourceInterface
{
    /**
     * @var string $file The path to the XML file.
     */
    protected $file;

    /**
     * Retrieve data from the XML file.
     *
     * @param string $file The path to the XML file.
     * @return array The data retrieved as an array of Coffee objects.
     * @throws \RuntimeException If the file does not exist, does not have the ".xml" extension, or cannot be loaded.
     */
    public function getData(string $file): array
    {
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);

        if (!file_exists($file)) {
            throw new \RuntimeException('The file does not exist.');
        }

        if (strtolower($fileExtension) !== 'xml') {
            throw new \RuntimeException('The file does not have the ".xml" extension.');
        }

        try {
            $xmlObject = simplexml_load_file($file);
            $dataArray = $this->convertToArray($xmlObject);
            return $dataArray;
        } catch (\Exception $e) {
            throw new \RuntimeException('Failed to load XML file: ' . $e->getMessage());
        }
    }

    /**
     * Convert XML data to an array of Coffee objects.
     *
     * @param SimpleXMLElement $xmlObject The XML data.
     * @return array The data as an array of Coffee objects.
     */
    public function convertToArray($xmlObject): array
    {
        $dataArray = [];
        foreach ($xmlObject as $key => $item) {
            $coffee = new Coffee();
            $coffee->entity_id = (string) $item->entity_id;
            $coffee->CategoryName = (string) $item->CategoryName;
            $coffee->sku = (string) $item->sku;
            $coffee->name = (string) $item->name;
            $coffee->description = (string) $item->description;
            $coffee->shortdesc = (string) $item->shortdesc;
            $coffee->price = (float) $item->price;
            $coffee->link = (string) $item->link;
            $coffee->image = (string) $item->image;
            $coffee->Brand = (string) $item->Brand;
            $coffee->Rating = (float) $item->Rating;
            $coffee->CaffeineType = (string) $item->CaffeineType;
            $coffee->Count = (int) $item->Count;
            $coffee->Flavored = $item->Flavored == 'Yes';
            $coffee->Seasonal = $item->Seasonal == 'Yes';
            $coffee->Instock = $item->Instock == 'Yes';
            $coffee->Facebook = $item->Facebook == 1;
            $coffee->IsKCup = $item->IsKCup == 1;
            $dataArray[] = $coffee;
        }
        return $dataArray;
    }
}
