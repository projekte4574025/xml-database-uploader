<?php

namespace App\Implementations;

use App\Interfaces\DataStorageInterface;
use App\Models\Coffee;

/**
 * Class DataStorageDB
 *
 * Implementation of DataStorageInterface for storing data in a database.
 */
class DataStorageDB implements DataStorageInterface
{
    /**
     * Upload the data to the database.
     *
     * @param array $dataArray The data to be uploaded.
     *
     * @return string A message indicating the result of the upload.
     *
     * @throws \RuntimeException
     */
    public function uploadToDB($dataArray): string
    {
        try {
            foreach ($dataArray as $item) {
                $coffee = Coffee::updateOrCreate(
                    ['entity_id' => (int)$item->entity_id],
                    [
                        'category_name' => (string)$item->CategoryName,
                        'sku' => (string)$item->sku,
                        'name' => (string)$item->name,
                        'description' => (string)$item->description,
                        'shortdesc' => (string)$item->shortdesc,
                        'price' => (float)$item->price,
                        'link' => (string)$item->link,
                        'image' => (string)$item->image,
                        'brand' => (string)$item->Brand,
                        'rating' => (float)$item->Rating,
                        'caffeine_type' => (string)$item->CaffeineType,
                        'count' => (int)$item->Count,
                        'flavored' => (bool)$item->Flavored,
                        'seasonal' => (bool)$item->Seasonal,
                        'instock' => (bool)$item->Instock,
                        'facebook' => (bool)$item->Facebook,
                        'is_kcup' => (bool)$item->IsKCup,
                    ]
                );
            }
            $changesMade = ($coffee->wasRecentlyCreated || $coffee->wasChanged());
            if ($changesMade) {
                return 'XML data imported. Written to DB';
            } else {
                return 'XML data imported. Could not be written to DB.';
            }
        } catch (\Exception $e) {
            throw new \RuntimeException('Error occurred during XML import: ' . $e->getMessage());
        }
    }
}
