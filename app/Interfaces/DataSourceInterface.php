<?php

namespace App\Interfaces;

/**
 * Interface DataSourceInterface
 *
 * Interface for data source implementations to retrieve data.
 */
interface DataSourceInterface
{
    /**
     * Retrieve data from the data source.
     *
     * @param string $file The path to the data source file.
     *
     * @return array The data retrieved from the data source.
     *
     * @throws \RuntimeException If the data cannot be retrieved.
     */
    public function getData(string $file): array;
}
