<?php

namespace App\Interfaces;

/**
 * Interface DataStorageInterface
 *
 * Interface for data storage implementations to upload data.
 */
interface DataStorageInterface
{
    /**
     * Upload the data to the database.
     *
     * @param array $data The data to be uploaded.
     *
     * @return string A message indicating the result of the upload.
     *
     * @throws \RuntimeException If the data cannot be uploaded.
     */
    public function uploadToDB(array $data): string;
}
