<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

use App\Interfaces\DataSourceInterface;
use App\Interfaces\DataStorageInterface;

class Importer extends Command
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'importer {file : The path to the XML file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reads an XML file and uploads it to a database';

    /**
     * The data source interface.
     *
     * @var DataSourceInterface
     */
    protected $dataSource;

    /**
     * The data storage interface.
     *
     * @var DataStorageInterface
     */
    protected $dataStorage;

    /**
     * Create a new command instance.
     *
     * @param DataSourceInterface $dataSource
     * @param DataStorageInterface $dataStorage
     */
    public function __construct(DataSourceInterface $dataSource, DataStorageInterface $dataStorage)
    {
        parent::__construct();
        $this->dataSource = $dataSource;
        $this->dataStorage = $dataStorage;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Command is being executed.');
        try {
            $file = $this->argument('file');
            $this->verifyDBConnection();
            $dataArray = $this->dataSource->getData($file);
            $result = $this->dataStorage->uploadToDB($dataArray);

            if (is_string($result)) {
                $this->info($result);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            $this->error('An error occurred: ' . $e->getMessage());
        }
    }

    /**
     * Verify the database connection.
     *
     * @return void
     *
     * @throws \RuntimeException
     */
    private function verifyDBConnection()
    {
        try {
            $pdo = DB::connection()->getPdo();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw new \RuntimeException('The connection to the database failed.');
        }
    }
}
